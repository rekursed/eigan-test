from collections import Counter
import os

from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render
from . import utils
from .forms import UploadFileForm


# Create your views here.
def taskPage(request):
    files = []
    for file in os.listdir(settings.FILE_ROOT):
        if file.endswith(".txt"):
            files.append(file)
    context = {'files': files, 'form': UploadFileForm()}
    if request.method == 'POST':
        fileNames = request.POST.getlist('fileNames')
        top_n = int(request.POST.get('top'))
        result = utils.readFiles(fileNames)
        # context['result'] = result
        word_counter = Counter(result['wordcount'])
        result_table = []
        for word, count in word_counter.most_common(top_n):
            result_table.append(
                {'word': word, 'files': result['in_files'][word], 'sentences': result['in_sentences'][word]})
        context['results'] = result_table
        return render(request, 'taskpage.html', context=context)
    return render(request, 'taskpage.html', context=context)


def fileUpload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        x = request.FILES['file']
        if form.is_valid():
            utils.handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/')
