from django.conf import settings
import os
import re
from nltk import data, download


def handle_uploaded_file(f):
    with open(os.path.join(settings.FILE_ROOT, f.name), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def readFiles(file_names: list):
    wordcount = {}
    in_files = {}
    in_sentences = {}
    tokenizer = None
    try:
        tokenizer = data.load('tokenizers/punkt/english.pickle')
    except:
        print('installing punkt')
        download('punkt')
    # Try to load again
    if not tokenizer:
        tokenizer = data.load('tokenizers/punkt/english.pickle')

    for file in file_names:
        text = open(os.path.join(settings.FILE_ROOT, file), encoding='utf8').read()
        sentence_list = tokenizer.tokenize(text)
        for sentence in sentence_list:
            for original_word in sentence.split():
                word = re.sub('[^a-z]', '', original_word.lower())
                if not word:
                    print(original_word)
                    continue
                if word not in wordcount:
                    wordcount[word] = 1
                else:
                    wordcount[word] += 1
                if word not in in_files:
                    in_files[word] = [file]
                else:
                    if file not in in_files[word]:
                        in_files[word].append(file)
                if word not in in_sentences:
                    in_sentences[word] = [sentence]
                else:
                    if sentence not in in_sentences[word]:
                        in_sentences[word].append(sentence)

    result = {'wordcount': wordcount, 'in_files': in_files, 'in_sentences': in_sentences}
    return result
