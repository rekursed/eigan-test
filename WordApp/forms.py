from django import forms


class UploadFileForm(forms.Form):
    file = forms.FileField()


class QueryForm(forms.Form):
    fileNames = forms.SelectMultiple()
    top = forms.IntegerField()
