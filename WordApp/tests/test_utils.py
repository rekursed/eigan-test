import os
from WordApp import utils


def test_sample():
    assert (1 == 1)


def test_read_file():
    test_file_path = f'{os.path.dirname(os.path.abspath(__file__))}/../../files/test_file.txt'
    with open(test_file_path, "w+") as test_file:
        test_file.write('to be or not to be to')
    result = utils.readFiles(['test_file.txt'])
    os.remove(test_file_path)
    assert result['wordcount']['to'] == 3