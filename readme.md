# Eigen Test

Coding test solution for interview

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

`python3` is required to run this app and
`venv` package in `pip` is required to add virtual environment

### Building


After checking out the repo, it is ideal to build the app using virtualenv

```
cd <code_direcrtory>
python3 -m venv venv
pip install -r requirements.txt

```

And to run the server

```
python manage.py runserver
```

Then visit `http://localhost:8000` to view the app.

## Running the tests

running the test can be done by 
```
cd <code_direcrtory>
pytest
```
currently there is only one test testing only the core function
